terraform {
	required_providers {
		oci = {
			source = "hashicorp/oci"
		}
	}
}

provider "oci"{
	region = var.region
}

resource "oci_core_vcn" "example_vcn" {
	dns_label	= "examplevcn"
	cidr_block	= "176.16.0.0/20"
	compartment_id	= var.compartment_id
	display_name	= "example VCN"
}

resource "oci_core_subnet" "example_subnet1" {
	vcn_id	= oci_core_vcn.mjemthiyas_vcn.id
	cidr_block	= "176.16.0.0/24"
	compartment_id	= var.compartment_id
	display_name	= "example subnet1"
	prohibit_public_ip_on_vnic	= false
	dns_label	= "examplesubnet1"
}

resource "oci_core_subnet" "example_subnet2" {
	vcn_id	= oci_core_vcn.mjemthiyas_vcn.id
	cidr_block	= "176.16.1.0/24"
	compartment_id	= var.compartment_id
	display_name	= "example subnet2"
	dns_label	= "examplesubnet2"
}