variable "compartment_id" {
	description = "OCID from tenancy page"
	type = string
}

variable "region" {
	description = "region from tenancy page"
	type = string
	default = "us-ashburn-1"
}